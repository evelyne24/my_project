import logging
import logging.config

import first_module
import second_module

from logger import logger

def main():
  logger.debug('Hello')

  first_module.meow()
  first_module.woof()

  second_module.honk()
  second_module.choo()

if __name__ == '__main__':
  # execute only if run as a script
  main()
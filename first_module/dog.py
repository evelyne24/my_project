import sys
from os import path
sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))

from logger import logger

def woof():
  logger.debug('Woof, Woof!') 

def main():
  woof()

if __name__ == '__main__':
  # execute only if run as a script
  main()
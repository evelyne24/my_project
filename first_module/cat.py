import sys
from os import path
sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))

from logger import logger

def meow():
  logger.debug('Meow, Meow!') 

def main():
  meow()

if __name__ == '__main__':
  # execute only if run as a script
  main()
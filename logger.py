import logging
import logging.handlers

formatter = logging.Formatter('%(asctime)s [%(levelname)-5.5s]: %(message)s - %(filename)s:%(lineno)d', datefmt='%d-%m %H:%M')

# logging.basicConfig(level=logging.DEBUG,
#                     format='%(asctime)s %(levelname)s %(message)s',
#                     filename='myproject.log',
#                     filemode='w')

# set up logging to console
console = logging.StreamHandler()
console.setLevel(logging.DEBUG)
console.setFormatter(formatter)

# set up logging to file
fileHandler = logging.handlers.RotatingFileHandler('myproject.log', 
  maxBytes=10 * 1000 * 1000, 
  encoding='utf8', 
  backupCount=5)
fileHandler.setFormatter(formatter)
fileHandler.setLevel(logging.DEBUG)

logger = logging.getLogger()
logger.setLevel(logging.DEBUG)
logger.addHandler(console)
logger.addHandler(fileHandler)
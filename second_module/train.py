import sys
from os import path
sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))

from logger import logger

def choo():
  logger.debug('Choo, Choo!') 

def main():
  choo()

if __name__ == '__main__':
  # execute only if run as a script
  main()
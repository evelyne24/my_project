import sys
from os import path
sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))

from logger import logger

def honk():
  logger.debug('Honk, Honk!') 

def main():
  honk()

if __name__ == '__main__':
  # execute only if run as a script
  main()